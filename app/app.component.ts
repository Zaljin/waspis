import {Component,OnInit} from '@angular/core';
import {FrontPageComponent} from './front-page/index'
import {TableComponent} from "./table/index"
import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS,Router } from '@angular/router-deprecated'

@Component({
    selector: 'sfs-app',
    templateUrl: 'app/app.component.html',
    directives:[FrontPageComponent, TableComponent,ROUTER_DIRECTIVES],
    providers: [ROUTER_PROVIDERS]
})

@RouteConfig([
  {
    path: '/table',
    name: 'Table',
    component: TableComponent
  },
  {
      path: '/front-page',
      name: 'Front Page',
      component: FrontPageComponent,
      useAsDefault: true
  }
])


export class AppComponent{ 
    constructor(private router:Router){}
}
