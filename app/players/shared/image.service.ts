import { Injectable } from '@angular/core';
import {MockImages} from './mockImages'

@Injectable()
export class ImageService {

    constructor() { }
    
    getImages(){
        return MockImages;
    }

}