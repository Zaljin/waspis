export class Image {
    id:number;
    alt:string = "";
    name:string = "";
    base64:string ='';
}