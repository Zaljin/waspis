import {Stat} from "../../stats/index"
import {Image} from "./index"

export class Player{
    id:number;
    name:string;
    stats:Stat[];
    images:Image[];
}