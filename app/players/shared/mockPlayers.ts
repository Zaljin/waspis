import {Player,MockImages} from "./index";
import {MockStats} from "../../stats/index"
export var MockPlayers:Player[] = [
  {id:1,name:'Drew',stats:[],images:[MockImages[2]]},
  {id:2,name:'Juan',stats:[],images:[MockImages[1]]},
  {id:3,name:'Mike',stats:[],images:[MockImages[5]]},
  {id:4,name:'Kent',stats:[],images:[MockImages[3]]},
  {id:5,name:'Sanju',stats:[],images:[MockImages[4]]},
  {id:6,name:'Eric',stats:[],images:[MockImages[0]]}
]