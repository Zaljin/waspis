import { Component, OnInit,Input } from '@angular/core';
import { Image } from '../../players/index';

@Component({
    moduleId: module.id,
    selector: 'sfs-image',
    templateUrl: 'image.component.html',
    styleUrls:['image.component.css']
})
export class ImageComponent implements OnInit {
    @Input() image:Image;
    constructor() { }

    ngOnInit() { }

}