import {Stat} from "./index"
export var MockStats:Stat[] = [
    {id:1,name:'Games Played',value:'0'},
    {id:2,name:'Games Won',value:'0'},
    {id:3,name:'Games Lost',value:'0'},
    {id:4,name:'Goals Scored',value:'0'},
    {id:5,name:'Goals Allowed',value:'0'}
    
]