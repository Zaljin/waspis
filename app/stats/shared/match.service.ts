import {Injectable} from '@angular/core';
import {MockMatches} from './index'

@Injectable()
export /**
 * MatchService
 */
class MatchService {
    constructor() {}
    
    getMatches(){
        return MockMatches;
    }
    
}