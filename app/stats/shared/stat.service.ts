import { Injectable } from '@angular/core';
import {MockStats} from './index'

@Injectable()
export class StatService {

    constructor() { }
    
    getStats(){
        return MockStats;
    }

}