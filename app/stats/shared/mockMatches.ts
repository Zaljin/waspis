import {Match} from "./index"
export var MockMatches:Match[] = [
    {id:1,date:new Date('05-16-2016'),position:'offense',goals:2,allowed:4,win:true,teamateId:2},
    {id:2,date:new Date('05-17-2016'),position:'offense',goals:6,allowed:4,win:true,teamateId:3},
    {id:3,date:new Date('05-18-2016'),position:'defense',goals:1,allowed:1,win:true,teamateId:2},
    {id:4,date:new Date('05-19-2016'),position:'offense',goals:3,allowed:0,win:false,teamateId:2},
    {id:5,date:new Date('05-20-2016'),position:'defense',goals:2,allowed:3,win:true,teamateId:2},
    {id:6,date:new Date('05-21-2016'),position:'offense',goals:2,allowed:4,win:true,teamateId:2}
]