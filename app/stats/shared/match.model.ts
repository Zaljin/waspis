export class Match{
    id:number;
    date:Date;
    position:string;
    goals:number;
    allowed:number;
    win:boolean;
    teamateId:number;
}