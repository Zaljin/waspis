import { Component, OnInit } from '@angular/core';
import {StatService,MockStats,Stat} from '../shared/index';
import {StatComponent} from '../stat/index';

@Component({
    selector: 'sfs-stat-list',
    templateUrl: 'app/stats/stats-list/stats-list.component.html',
    styleUrls:['app/stats/stats-list/stats-list.component.css'],
    //providers:[StatsService],
    directives:[StatComponent]
})

export class StatsListComponent implements OnInit {
    stats:Stat[];
    statsService:StatService;
    
    //constructor(private _statsService:StatsService ) {this.statsService=_statsService}

    ngOnInit() { 
        this.stats=MockStats;
    }

}