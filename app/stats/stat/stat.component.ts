import { Component, OnInit,Input } from '@angular/core';
import {Stat} from '../shared/index'

@Component({
    selector: 'sfs-stat',
    templateUrl: 'app/stats/stat/stat.component.html'
})

export class StatComponent implements OnInit {
    @Input() stat: Stat;
    constructor() { }

    ngOnInit() { }

}