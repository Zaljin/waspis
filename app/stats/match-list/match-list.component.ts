import { Component, OnInit } from '@angular/core';
import { Match, MatchService, MockMatches } from '../shared/index';
import {MatchComponent} from '../match/index';


@Component({
    selector: 'sfs-match-list',
    templateUrl: 'app/stats/match-list/match-list.component.html',
    directives:[MatchComponent]
})
export class MatchListComponent implements OnInit {
    matches:Match[];
    matchesService:MatchService;
    
    constructor() { }

    ngOnInit() { 
        this.matches=MockMatches;
    }

}