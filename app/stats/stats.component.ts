import { Component, OnInit } from '@angular/core';
import {StatsListComponent} from './index';
import {MatchHistoryComponent} from './match-history.component';
import {Image,MockImages} from '../players/shared/index';

@Component({
  selector: 'sfs-stats',
  templateUrl: 'app/stats/stats.component.html',
  directives:[StatsListComponent, MatchHistoryComponent]
})

export class StatsComponent implements OnInit {
  images:Image[];
  show:boolean = false;
  
  constructor() { }

  ngOnInit() { 
    //this.images=MockImages;
  }
}