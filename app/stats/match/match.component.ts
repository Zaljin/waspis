import { Component, OnInit,Input } from '@angular/core';
import {Match} from '../shared/index'

@Component({
    selector: 'sfs-match',
    templateUrl: 'app/stats/match/match.component.html'
})
export class MatchComponent implements OnInit {
    @Input() match:Match;
    constructor() { }

    ngOnInit() { }

}