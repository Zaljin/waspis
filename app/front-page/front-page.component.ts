import { Component, OnInit} from '@angular/core';
import {StatsComponent} from "../stats/index"
import {Router} from '@angular/router-deprecated'

@Component({
    selector: 'sfs-front-page',
    templateUrl: 'app/front-page/front-page.component.html',
    directives:[StatsComponent]
})
export class FrontPageComponent implements OnInit {
    constructor(private router:Router) {}

    onCreateButtonClick(){
        this.router.navigate(['Table']);
    }
    
    ngOnInit() { }

}