import { Component, OnInit } from '@angular/core';

import { MockImages } from '../../players/shared/index';
import { Image } from '../../players/shared/image.model';

import { Dragula, DragulaService } from 'ng2-dragula/ng2-dragula';

@Component({
  selector: 'sfs-dnd',
  templateUrl : 'app/shared/drag-n-drop-example/dnd.component.html',
  directives: [Dragula],
  viewProviders: [DragulaService],
  styleUrls: ['app/shared/drag-n-drop-example/dnd.component.css']
})
export class DragAndDropComponent implements OnInit {
  playerSelection: string = 'player-bag';
  public images: Image[] = [];
  
  constructor(private dragulaService: DragulaService) {
    this.images = MockImages;
    
    dragulaService.drag.subscribe((value: any) => {
      console.log(`drag: ${value[0]}`);
      this.onDrag(value.slice(1));
    });
    dragulaService.drop.subscribe((value: any) => {
      console.log(`drop: ${value[0]}`);
      this.onDrop(value.slice(1));
    });
    dragulaService.over.subscribe((value: any) => {
      console.log(`over: ${value[0]}`);
      this.onOver(value.slice(1));
    });
    dragulaService.out.subscribe((value: any) => {
      console.log(`out: ${value[0]}`);
      this.onOut(value.slice(1));
    });
    dragulaService.dropModel.subscribe((value: any) => {
      this.onDropModel(value.slice(1));
    });
    dragulaService.removeModel.subscribe((value: any) => {
      this.onRemoveModel(value.slice(1));
    });
  }
  
  private limit(el: any, target: any, source: any, sibling: any) {
    console.log(el);
    console.log(target);
    console.log(source);
    console.log(sibling);
    console.log('----------');
    return true;
    //return !(target.className.indexOf('deny') > -1);
  }
  
  private onDropModel(args: any) {
    let [el, target, source] = args;
    // do something else
    el.className += ' ex-moved';
    if (target.className.indexOf('deniable') > -1) {
      if (target.className.indexOf('deny') > -1) {
        target.remove();
      }
      else
        target.className += ' deny';
    }
  }

  private onRemoveModel(args: any) {
    let [el, source] = args;
    // do something else
  }
  
  // e is the element being moved
  private onDrag(args: any) {
    let [e, el] = args;
    e.className = e.className.replace(' ex-moved', '');
    if (el.className.indexOf('deniable') > -1)
      el.className = el.className.replace(' deny', '');
  }

  // e is the element being moved
  private onDrop(args: any) {
    let [e, el, target, source] = args;
    e.className += ' ex-moved';
    if (el.className.indexOf('deniable') > -1) {      
      if (el.childElementCount > 1) {
        var swap = el.firstChild;
        swap.remove();
        target.appendChild(swap);
      }
      else
        el.className += ' deny';
    }
  }

  // e is the element being moved
  private onOver(args: any) {
    let [e, el, container] = args;
    e.className += ' ex-over';
  }

  // e is the element being moved
  private onOut(args: any) {
    let [e, el, container] = args;
    e.className = e.className.replace(' ex-over', '');
  }
  
  ngOnInit() {
    
  }
}