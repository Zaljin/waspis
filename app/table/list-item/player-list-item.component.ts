import { Component, OnInit,Input } from '@angular/core';
import {Player,ImageComponent} from "../../players/index";

@Component({
    moduleId: module.id,
    selector: 'sfs-player-list-item',
    templateUrl: 'player-list-item.component.html',
    directives:[ImageComponent],
    styleUrls:['player-list-item.component.css']
})
export class PlayerListItemComponent implements OnInit {
    @Input() player:Player;
    constructor() { }

    ngOnInit() { }

}