import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {PlayerService, Player, MockPlayers, ImageComponent} from '../players/index';
import {Router} from '@angular/router-deprecated'
import {PlayerListItemComponent} from './list-item/player-list-item.component'

import { DragAndDropComponent } from '../shared/drag-n-drop-example/dnd.component';
import { Dragula, DragulaService } from 'ng2-dragula/ng2-dragula';

@Component({
  selector: 'sfs-table',
  templateUrl: 'app/table/table.component.html',
  directives:[ImageComponent, PlayerListItemComponent, DragAndDropComponent, Dragula],
  viewProviders: [DragulaService],
  styleUrls: ['app/table/table.component.css']
})
export class TableComponent implements OnInit{
  playersReady:number=0;
  leftDefense:boolean=false;
  rightDefense:boolean=false;
  leftOffense:boolean=false;
  rightOffense:boolean=false;
  players:Player[];
  PlayerService:PlayerService;
  @Output() onBackClicked=new EventEmitter<boolean>();
  //stats:Stat[];
  //statsService:StatsService;
  
  //constructor(private _statsService:StatsService ) {this.statsService=_statsService}
  constructor(private router:Router, private dragulaService: DragulaService) {
    dragulaService.drag.subscribe((value: any) => {
      console.log(`drag: ${value[0]}`);
      this.onDrag(value.slice(1));
    });
    dragulaService.drop.subscribe((value: any) => {
      console.log(`drop: ${value[0]}`);
      this.onDrop(value.slice(1));
    });
    dragulaService.over.subscribe((value: any) => {
      console.log(`over: ${value[0]}`);
      this.onOver(value.slice(1));
    });
    dragulaService.out.subscribe((value: any) => {
      console.log(`out: ${value[0]}`);
      this.onOut(value.slice(1));
    });
    dragulaService.dropModel.subscribe((value: any) => {
      this.onDropModel(value.slice(1));
    });
    dragulaService.removeModel.subscribe((value: any) => {
      this.onRemoveModel(value.slice(1));
    });
  }
  
  
  private onDropModel(args: any) {
    let [el, target, source] = args;
    // do something else
    el.className += ' ex-moved';
    if (target.className.indexOf('deniable') > -1) {
      if (target.className.indexOf('deny') > -1) {
        target.remove();
      }
      else
        target.className += ' deny';
    }
  }

  private onRemoveModel(args: any) {
    let [el, source] = args;
    // do something else
  }
  
  // e is the element being moved
  private onDrag(args: any) {
    let [e, el] = args;
    e.className = e.className.replace(' ex-moved', '');
    if (el.className.indexOf('deniable') > -1)
      el.className = el.className.replace(' deny', '');
  }

  // e is the element being moved
  private onDrop(args: any) {
    let [e, el, target, source] = args;
    
    e.className += ' ex-moved';
    if (el.className.indexOf('deniable') > -1) {      
      if (el.childElementCount > 1) {
        var swap = el.firstChild;
        swap.remove();
        target.appendChild(swap);
      }
      else{        
        el.className += ' deny';
      }      
      this.setPlayer(target,el);//I know they look reversed, it works
    }
    
  }

  // e is the element being moved
  private onOver(args: any) {
    let [e, el, container] = args;
    e.className += ' ex-over';
  }

  // e is the element being moved
  private onOut(args: any) {
    let [e, el, container] = args;
    e.className = e.className.replace(' ex-over', '');
  }

  ngOnInit() { 
      this.players=MockPlayers;
  }
  
  onBackClick(){
    this.router.navigate(['Front Page']);
  }
  
  getBagName(element:any){
    return element.getAttribute('name');
  }
  setPlayer(source:any,target:any){
    let sourceClass=this.getBagName(source);
    let targetClass=this.getBagName(target);
    console.log("Source:"+sourceClass);
    console.log("Target:"+targetClass);
    switch (targetClass) {
      case 'leftDefense':
        this.leftDefense=true; 
        this.playersReady++;       
        break;
      case 'leftOffense':
        this.leftOffense=true;
        this.playersReady++;       
        break;
      case 'rightDefense':
        this.rightDefense=true;
        this.playersReady++;       
        break;
      case 'rightOffense':
        this.rightOffense=true;
        this.playersReady++;       
        break;
      default:
        break;
    }
    switch (sourceClass) {
      case 'leftDefense':
        this.leftDefense=false;
        this.playersReady--;               
        break;
      case 'leftOffense':
        this.leftOffense=false;
        this.playersReady--;       
        break;
      case 'rightDefense':
        this.rightDefense=true;
        this.playersReady--;       
        break;
      case 'rightOffense':
        this.rightOffense=true;
        this.playersReady--;       
        break;
      default:
        break;
    }    
    console.log("Players:"+this.playersReady);
  }
  
  isGameNotReady(){
    return this.playersReady!=4
  }
}
